"""
.. module:: MetricAccuracy
   :synopsis: Performance Metric: AccuracyReject

"""

from secml.array import CArray
from secml.ml.peval.metrics import CMetric, CMetricAccuracy


class CMetricAccuracyReject(CMetric):
    """Performance evaluation metric: Accuracy.

    Accuracy score is the percentage (inside 0/1 range)
    of correctly predicted labels. Rejected samples are
    considered as a correct prediction.

    The metric uses:
     - y_true (true ground labels)
     - y_pred (predicted labels)

    Attributes
    ----------
    class_type : 'accuracy-rej'

    """
    __class_type = 'accuracy-reject'
    best_value = 1.0

    def _performance_score(self, y_true, y_pred):
        """Computes the Accuracy score.

        Parameters
        ----------
        y_true : CArray
            Ground truth (true) labels or target scores.
        y_pred : CArray
            Predicted labels, as returned by a CClassifier.

        Returns
        -------
        metric : float
            Returns metric value as float.

        """
        acc_metric = CMetricAccuracy()

        # rejected samples are considered as correct predictions
        is_rej = y_pred == -1
        y_true_mod = y_true.deepcopy()
        y_true_mod[is_rej] = -1

        # check the classifier accuracy
        acc = acc_metric.performance_score(y_true=y_true_mod, y_pred=y_pred)

        return acc
