from collections import OrderedDict

import torch
from torch import nn, optim

from secml.ml import CClassifierPyTorch
from secml.ml.classifiers.reject.tests import CClassifierRejectTestCases
from secml.ml.classifiers.reject import CClassifierDNR
from secml.ml.peval.metrics import CMetric
from secml.data.loader import CDataLoaderMNIST
from secml.data.splitter import CTrainTestSplit
from secml.ml.classifiers import CClassifierSVM
from secml.ml.classifiers.multiclass import CClassifierMulticlassOVA
from secml.ml.kernels import CKernelRBF


class TestCClassifierDNR(CClassifierRejectTestCases):
    """Unit test for CClassifierRejectThreshold."""

    @classmethod
    def setUpClass(cls):

        CClassifierRejectTestCases.setUpClass()

        # Create the classifier
        cls.clf, cls.tr, cls.ts = cls._create_clf()
        cls.clf.fit(cls.tr.X, cls.tr.Y)

    @staticmethod
    def _create_clf():
        """Load a pretrained RESNET18 network and create the wrapping clf."""
        digits = (1, 4, 5, 9)
        ds = CDataLoaderMNIST().load('training', digits=digits)

        # Split training set for dnn and for DNR classifier and test set
        tr_dnn, ds_dnr = CTrainTestSplit(
            train_size=300, test_size=350, random_state=0).split(ds)
        tr_dnr, ts_dnr = CTrainTestSplit(
            train_size=300, test_size=50, random_state=0).split(ds_dnr)

        tr_dnn.X /= 255.
        tr_dnr.X /= 255.
        ts_dnr.X /= 255.

        torch.manual_seed(0)

        class Flatten(nn.Module):
            def forward(self, input):
                return input.view(input.size(0), -1)

        od = OrderedDict([
            ('conv1', nn.Conv2d(1, 10, kernel_size=5)),
            ('pool1', nn.MaxPool2d(2)),
            ('conv2', nn.Conv2d(10, 20, kernel_size=5)),
            ('drop', nn.Dropout2d()),
            ('pool2', nn.MaxPool2d(2)),
            ('flatten', Flatten()),
            ('fc1', nn.Linear(320, 50)),
            ('relu', nn.ReLU()),
            ('fc2', nn.Linear(50, 4)),
        ])

        net = nn.Sequential(OrderedDict(od))
        criterion = nn.CrossEntropyLoss()
        optimizer = optim.SGD(net.parameters(), lr=0.1, momentum=0.9)
        scheduler = optim.lr_scheduler.MultiStepLR(
            optimizer=optimizer, milestones=[1, 5, 8], gamma=0.1)

        dnn = CClassifierPyTorch(
            model=net, loss=criterion, optimizer=optimizer, epochs=10,
            batch_size=20, input_shape=(1, 28, 28),
            optimizer_scheduler=scheduler, random_state=0)
        dnn.fit(tr_dnn.X, tr_dnn.Y)
        layers = ['conv2', 'relu']
        combiner = CClassifierMulticlassOVA(
            CClassifierSVM, kernel=CKernelRBF(gamma=1), C=1)
        layer_clf = CClassifierMulticlassOVA(
            CClassifierSVM, kernel=CKernelRBF(gamma=1), C=1)

        return CClassifierDNR(combiner, layer_clf, dnn, layers, -1000), \
            tr_dnr, ts_dnr

    def test_fun(self):
        """Test for decision_function() and predict() methods."""
        self.logger.info(
            "Test for decision_function() and predict() methods.")

        scores_d = self._test_fun(self.clf, self.ts.todense())
        scores_s = self._test_fun(self.clf, self.ts.tosparse())

        self.assert_array_almost_equal(scores_d, scores_s)

        y_pred = self.clf.predict(self.ts.X)
        accuracy = (self.ts.Y == y_pred).mean()
        self.logger.info("Accuracy: {:}".format(accuracy))

    def _test_fun(self, clf, ds):
        self.logger.info(
            "Test for decision_function() and predict() methods.")

        if ds.issparse:
            self.logger.info("Testing on sparse data...")
        else:
            self.logger.info("Testing on dense data...")

        # we have to ensure at least 2d here, since _decision_function is not
        # applying this change anymore (while decision_function does).
        x = x_norm = ds.X.atleast_2d()
        p = p_norm = ds.X[0, :].ravel().atleast_2d()

        # Transform data if preprocess is defined
        if clf.preprocess is not None:
            x_norm = clf.preprocess.forward(x)
            p_norm = clf.preprocess.forward(p)

        # Testing decision_function on multiple points
        df, df_priv = [], []
        for y in range(ds.num_classes):
            df.append(clf.decision_function(x, y=y))
            df_priv.append(clf._forward(x_norm)[:, y].ravel())
            self.logger.info(
                "decision_function(x, y={:}): {:}".format(y, df[y]))
            self.logger.info(
                "_decision_function(x_norm, y={:}): {:}".format(y, df_priv[y]))
            self._check_df_scores(df_priv[y], ds.num_samples)
            self._check_df_scores(df[y], ds.num_samples)
            self.assertFalse((df[y] != df_priv[y]).any())

        # Testing predict on multiple points
        labels, scores = clf.predict(
            x, return_decision_function=True)
        self.logger.info(
            "predict(x):\nlabels: {:}\nscores: {:}".format(labels, scores))
        self._check_classify_scores(
            labels, scores, ds.num_samples, clf.n_classes)

        # Comparing output of decision_function and predict
        for y in range(ds.num_classes):
            self.assertFalse((df[y] != scores[:, y].ravel()).any())

        # Testing decision_function on single point
        df, df_priv = [], []
        for y in range(ds.num_classes):
            df.append(clf.decision_function(p, y=y))
            df_priv.append(clf._forward(p_norm)[:, y].ravel())
            self.logger.info(
                "decision_function(p, y={:}): {:}".format(y, df[y]))
            self._check_df_scores(df[y], 1)
            self.logger.info(
                "_decision_function(p_norm, y={:}): {:}".format(y, df_priv[y]))
            self._check_df_scores(df_priv[y], 1)
            self.assertFalse((df[y] != df_priv[y]).any())

        self.logger.info("Testing predict on single point")

        labels, scores = clf.predict(
            p, return_decision_function=True)
        self.logger.info(
            "predict(p):\nlabels: {:}\nscores: {:}".format(labels, scores))
        self._check_classify_scores(labels, scores, 1, clf.n_classes)

        # Comparing output of decision_function and predict
        for y in range(ds.num_classes):
            self.assertFalse((df[y] != scores[:, y].ravel()).any())

        return scores

    def test_reject(self):

        y_pred, score_pred = self.clf.predict(
            self.ts.X, return_decision_function=True)
        # set the threshold to have 10% of rejection rate
        threshold = self.clf.compute_threshold(0.1, self.ts)
        self.clf.threshold = threshold
        y_pred_reject, score_pred_reject = self.clf.predict(
            self.ts.X, return_decision_function=True)

        # Compute the number of rejected samples
        n_rej = (y_pred_reject == -1).sum()
        self.logger.info("Rejected samples: {:}".format(n_rej))

        self.logger.info("Real: \n{:}".format(self.ts.Y))
        self.logger.info("Predicted: \n{:}".format(y_pred))
        self.logger.info(
            "Predicted with reject: \n{:}".format(y_pred_reject))

        acc = CMetric.create('accuracy').performance_score(
            y_pred, self.ts.Y)
        self.logger.info("Accuracy no rejection: {:}".format(acc))

        rej_acc = CMetric.create('accuracy').performance_score(
            y_pred_reject[y_pred_reject != -1],
            self.ts.Y[y_pred_reject != -1])
        self.logger.info("Accuracy with rejection: {:}".format(rej_acc))

        # check that the accuracy using reject is higher that the one
        # without rejects
        self.assertGreaterEqual(
            rej_acc, acc, "The accuracy of the classifier that is allowed "
                          "to reject is lower than the one of the "
                          "classifier that is not allowed to reject")

    def test_set_params(self):
        """Test for setting layer classifiers parameters."""

        self.clf.set_params({'conv2.C': 10, 'conv2.kernel.gamma': 20})
        self.clf.set('relu.C', 20)

        for clf in self.clf._layer_clfs['conv2']._binary_classifiers:
            self.assertEqual(clf.C, 10.0)
            self.assertEqual(clf.kernel.gamma, 20.0)

        for clf in self.clf._layer_clfs['relu']._binary_classifiers:
            self.assertEqual(clf.C, 20.0)


if __name__ == '__main__':
    CClassifierRejectTestCases.main()
