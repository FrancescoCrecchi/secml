import time

from secml.array import CArray
from secml.ml import CClassifier
from secml.ml.classifiers.reject import CClassifierRejectThreshold
from secml.ml.classifiers import CClassifierDNN
from secml.ml.features.normalization import CNormalizerDNN
from secml.core.attr_utils import add_readwrite
from secml.parallel import parfor
import math


# TODO: complete docstrings


class CClassifierDNR(CClassifierRejectThreshold):
    """

    Parameters
    ----------
    combiner : CClassifier

    layer_clf : CClassifier

    dnn : CClassifierDNN

    layers : list of str

    threshold : float

    n_jobs : int, optional

    """
    __class_type = 'dnr'

    def __init__(self, combiner, layer_clf, dnn, layers, threshold, n_jobs=1):

        self.n_jobs = n_jobs
        super(CClassifierDNR, self).__init__(combiner, threshold)

        if not isinstance(dnn, CClassifierDNN):
            raise TypeError("`dnn` must be an instance of `CClassifierDNN`")
        if not isinstance(layer_clf, CClassifier):
            raise TypeError("`layer_clf` must be an instance of `CClassifier`")
        if not isinstance(layers, list):
            raise TypeError("`layers` must be a list")

        self._layers = layers
        self._layer_clfs = {}
        internal_params = {}
        for layer in self._layers:
            self._layer_clfs[layer] = layer_clf.deepcopy()
            # search for nested preprocess modules until the inner is reached
            module = self._layer_clfs[layer]
            while module.preprocess is not None:
                module = module.preprocess
            # once the inner preprocess is found, append the dnn to it
            module.preprocess = CNormalizerDNN(net=dnn, out_layer=layer)
            # this allows to access inner classifiers using the
            # respective layer name
            add_readwrite(self, layer, self._layer_clfs[layer])

    def _fit(self, x, y):
        """Extract the scores from layer classifiers and train the combiner.

        Parameters
        ----------
        x : CArray
            Array to be used for training with shape (n_samples, n_features).
        y : CArray
            Array of shape (n_samples,) containing the class labels.

        Returns
        -------
        trained_clf : CClassifier
            Instance of the classifier trained using input dataset.

        """
        x = self._create_scores_dataset(x, y)
        self._clf.fit(x, y)
        return self

    def _create_scores_dataset(self, x, y):
        """Extract validation scores from layer classifiers using the
        `fit_forward` function, based on a cross validation

        Parameters
        ----------
        x : CArray
            Array to be used for training with shape (n_samples, n_features).
        y : CArray
            Array of shape (n_samples,) containing the class labels.

        Returns
        -------
        concat_scores : CArray
            Concatenated layer classifiers scores, shape
            (n_samples, n_classes - 1), where n_classes includes
            the reject class.

        See Also
        --------
        :meth:`.CClassifier.fit_forward`

        """
        n_classes = y.unique().size
        # array that contains concatenate scores of layer classifiers
        concat_scores = CArray.zeros(
            shape=(x.shape[0], n_classes * len(self._layers)))

        for i, layer in enumerate(self._layers):
            scores = self._layer_clfs[layer].fit_forward(x, y)
            concat_scores[:, i * n_classes: n_classes + i * n_classes] = scores

        # def ffwd(params):
        #     l, x, y = params
        #     return l.fit_forward(x, y)
        #
        # args = zip(self._layer_clfs.values(), [x] * len(self._layers), [y] * len(self._layers))
        # concat_scores = parfor(ffwd, self.n_jobs, args)

        return concat_scores

    def _get_layer_clfs_scores(self, x):
        """

        Parameters
        ----------
        x : CArray
            Array with new patterns to classify, 2-Dimensional of shape
            (n_patterns, n_features).

        Returns
        -------
        concat_scores : CArray
            Concatenated layer classifiers scores, shape
            (n_samples, n_classes - 1), where n_classes includes
            the reject class.

        """
        caching = self._cached_x is not None

        n_classes = self.n_classes - 1
        # array that contains concatenate scores of layer classifiers
        concat_scores = CArray.zeros(
            shape=(x.shape[0], n_classes * len(self._layers)))
        for i, l in enumerate(self._layers):
            scores = self._layer_clfs[l].forward(x, caching=caching)
            concat_scores[:, i * n_classes: n_classes + i * n_classes] = scores

        # fwd = lambda l: l.forward(x, caching=caching)
        # concat_scores = parfor(fwd, self.n_jobs, self._layers)

        return concat_scores

    def _forward(self, x):
        """"Private method that computes the decision function.

        Parameters
        ----------
        x : CArray
            Array with new patterns to classify, 2-Dimensional of shape
            (n_patterns, n_features).

        Returns
        -------
        scores : CArray
            Array of shape (n_patterns, n_classes) with classification
            score of each test pattern with respect to each training class.
            Will be returned only if `return_decision_function` is True.

        """
        caching = self._cached_x is not None

        # DEBUG: [PROFILING]
        _start = time.time()
        layer_clfs_scores = self._get_layer_clfs_scores(x)
        self.logger.debug("[PROFILING] FWD: 'preparing_scores' took {:.2f} s".format(time.time()-_start))

        _start = time.time()
        scores = self._clf.forward(layer_clfs_scores, caching=caching)
        self.logger.debug("[PROFILING] FWD: 'combiner.forward' took {:.2f} s".format(time.time() - _start))

        # augment score matrix with reject class scores
        rej_scores = CArray.ones(x.shape[0]) * self.threshold
        scores = scores.append(rej_scores.T, axis=1)
        return scores

    def _backward(self, w):
        """Compute the gradient of the classifier output wrt input.

        Parameters
        ----------
        w : CArray or None
            if CArray, it is pre-multiplied to the gradient
            of the module, as in standard reverse-mode autodiff.

        Returns
        -------
        gradient : CArray
            Accumulated gradient of the module wrt input data.
        """
        grad = CArray.zeros(self.n_features)

        # the derivative w.r.t. the rejection class is zero, thus we can just
        # call the combiner gradient by removing the last element from w.

        _start = time.time()
        grad_combiner = self._clf.backward(w[:-1])
        self.logger.debug("[PROFILING] BWD: combiner.backward() took {:.2f} s".format(time.time()-_start))

        _start = time.time()
        n_classes = self.n_classes - 1
        for i, l in enumerate(self._layers):
            # backward pass to layer clfs of their respective w
            grad += self._layer_clfs[l].backward(
                w=grad_combiner[i * n_classes: i * n_classes + n_classes])
        self.logger.debug("[PROFILING] BWD: 'backprop_to_layer_clfs' took {:.2f} s".format(time.time() - _start))
        return grad

    @property
    def _grad_requires_forward(self):
        """Returns True if gradient requires calling forward besides just
        computing the pre-processed input x. This is useful for modules that
        use auto-differentiation, like PyTorch, or if caching is required
        during the forward step (e.g., in exponential kernels).
        It is False by default for modules in this library, as we compute
        gradients analytically and only require the pre-processed input x."""
        return True
