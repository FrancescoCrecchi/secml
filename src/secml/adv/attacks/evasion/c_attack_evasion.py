"""
.. module:: CAttackEvasion
   :synopsis: Interface for evasion attacks

.. moduleauthor:: Battista Biggio <battista.biggio@unica.it>
.. moduleauthor:: Ambra Demontis <ambra.demontis@unica.it>
.. moduleauthor:: Marco Melis <marco.melis@unica.it>

"""
from abc import ABCMeta

from secml.parallel import parfor2
from secml.adv.attacks import CAttack, CAttackMixin
from secml.array import CArray
from secml.data import CDataset


def opt(i, attack, idx, x_init, x, y, verbose=0):

    attack.verbose = verbose

    k = idx[i].item()  # idx of sample that can be modified

    xi = x[k, :] if x_init is None else x_init[k, :]
    x_opt, f_opt = attack._run(x[k, :], y[k], x_init=xi)

    attack.logger.info(
        "Point: {:}/{:}, f(x):{:}".format(k, x.shape[0], f_opt))

    return x_opt, f_opt


class CAttackEvasion(CAttackMixin, CAttack, metaclass=ABCMeta):
    """Interface for Evasion attacks.

    Parameters
    ----------
    classifier : CClassifier
        Target classifier.
    surrogate_classifier : CClassifier
        Surrogate classifier, assumed to be already trained.
    surrogate_data : CDataset or None, optional
        Dataset on which the the surrogate classifier has been trained on.
        Is only required if the classifier is nonlinear.
    y_target : int or None, optional
            If None an error-generic attack will be performed, else a
            error-specific attack to have the samples misclassified as
            belonging to the `y_target` class.

    """
    __super__ = 'CAttackEvasion'

    def __init__(self, classifier, surrogate_classifier, surrogate_data=None, distance=None, dmax=None, lb=None,
                 ub=None, discrete=False, y_target=None, attack_classes='all', solver_type=None, solver_params=None):
        # HACK: Setting n_jobs default to 8
        self.n_jobs = 1
        super().__init__(classifier, surrogate_classifier, surrogate_data, distance, dmax, lb, ub, discrete, y_target,
                         attack_classes, solver_type, solver_params)

    ###########################################################################
    #                              PUBLIC METHODS
    ###########################################################################

    def run(self, x, y, ds_init=None, *args, **kargs):
        """Runs evasion on a dataset.

        Parameters
        ----------
        x : CArray
            Data points.
        y : CArray
            True labels.
        ds_init : CDataset
            Dataset for warm starts.

        Returns
        -------
        y_pred : CArray
            Predicted labels for all ds samples by target classifier.
        scores : CArray
            Scores for all ds samples by target classifier.
        adv_ds : CDataset
            Dataset of manipulated samples.
        f_obj : float
            Mean value of the objective function computed on each data point.

        """
        x = CArray(x).atleast_2d()
        y = CArray(y).atleast_2d()
        x_init = None if ds_init is None else CArray(ds_init.X).atleast_2d()

        # HACK: Speeding-up attacks by avoiding computing already evaded (@previous dmax) points (A. Sotgiu)
        if x_init is None:
            y_pred = self._classifier.predict(x)
            adv_ds = CDataset(x.deepcopy(), y.deepcopy())
        else:
            y_pred = self._classifier.predict(x_init)
            adv_ds = CDataset(x_init.deepcopy(), y.deepcopy())

        not_evaded = (y_pred == y).logical_or(y_pred == -1)

        # only consider samples that can be manipulated
        v = self.is_attack_class(y)
        v = v.logical_and(not_evaded)
        idx = CArray(v.find(v)).ravel()

        # # only consider samples that can be manipulated
        # v = self.is_attack_class(y)
        # idx = CArray(v.find(v)).ravel()
        # print(v, idx)

        # number of modifiable samples
        n_mod_samples = idx.size

        # array in which the value of the optimization function are stored
        fs_opt = CArray.zeros(n_mod_samples, )

        out_opt = parfor2(opt, n_mod_samples, self.n_jobs,
                          self, idx, x_init, x, y, self.verbose)

        for i in range(n_mod_samples):
            adv_ds.X[idx[i].item(), :] = out_opt[i][0]
            fs_opt[i] = out_opt[i][1]

        y_pred, scores = self.classifier.predict(
            adv_ds.X, return_decision_function=True)

        y_pred = CArray(y_pred)

        # Return the mean objective function value on the evasion points
        f_obj = fs_opt.mean()

        return y_pred, scores, adv_ds, f_obj
